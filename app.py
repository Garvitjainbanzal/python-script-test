import os
import re
from collections import defaultdict

def parse_pod_yml(filepath):
    """Parses a pod.yml file and returns a dictionary of database details."""
    database_details = {}
    with open(filepath, 'r') as f:
        for line in f:
            # Regex to match database entries
            match = re.search(r"(?i)(name:\s+)([^\s]+)(.*?(uri|host):\s+)([^,\s]+)", line)
            if match:
                database_name = match.group(2)
                database_uri = match.group(5)
                if database_name not in database_details:
                    database_details[database_name] = {"uri": database_uri}
                else:
                    # Handle multiple entries for the same database
                    existing_uri = database_details[database_name]["uri"]
                    if existing_uri != database_uri:
                        print(f"Warning: Duplicate database name {database_name} with different URIs found in {filepath}")
    return database_details

def collect_database_details(root_dir, output_file):
    """Collects database details from pod.yml files and writes them to a file."""
    all_details = defaultdict(list)
    for root, _, files in os.walk(root_dir):
        for filename in files:
            if filename == "pod.yml":
                filepath = os.path.join(root, filename)
                try:
                    details = parse_pod_yml(filepath)
                    for name, info in details.items():
                        all_details[name].append(info)
                except Exception as e:
                    print(f"Error parsing {filepath}: {e}")
    with open(output_file, 'w') as f:
        f.write("Database Name, URI\n")
        for name, details in all_details.items():
            for detail in details:
                f.write(f"{name},{detail['uri']}\n")

if __name__ == "__main__":
    # Replace with your actual values
    root_dir = "/path/to/your/private/repository"
    output_file = "database_details.txt"
    collect_database_details(root_dir, output_file)
    print(f"Database details collected and written to {output_file}")
